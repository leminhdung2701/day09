<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<link rel="stylesheet" href="css.css" type="text/css">

<div>
    <h1>Kết quả</h1>
</div>

<?php
$keys = array(
    1 => "a",
    2 => "b",
    3 => "a",
    4 => "c",
    5 => "d",

    6 => "a",
    7 => "b",
    8 => "a",
    9 => "c",
    10 => "d",
);

$point = 0;
$results = array();

if(isset($_COOKIE['cookie_ans_1'])&& isset($_COOKIE['cookie_ans_2'])) {
    $results_1 =  json_decode($_COOKIE['cookie_ans_1'], true);
    $results_2 =  json_decode($_COOKIE['cookie_ans_2'], true);
    $results = array_merge($results_1,$results_2);
    echo '<div><h3>' . "Đáp án của bạn: " . '</div>';
    for($i = 0; $i < count($results); $i++) {
        if ($results[$i] == $keys[$i+1]) {
            echo '<div><h4 style="">'. "Câu " . $i+1 . ": " . strtoupper($results[$i]) . '</h4></div>';
            $point += 1;
        }else{
            if ($results[$i] == "0") {
                echo '<div><h4 style="color:blue">'. "Câu " . $i +1 . ": Chưa chọn đáp án". '</h4></div>';
            }else{
                echo '<div><h4 style="color:red">' . "Câu " . $i+1 . ": " . strtoupper($results[$i]) .'</h4></div>';
            }
        }
    }
}

echo '<div><h2>' . "Tổng điểm của bạn là: " . $point ."/10.". '</div>';
if($point<4){
    echo '<div><h2>' . "Nhận xét: Bạn quá kém, cần ôn tập thêm.".'</div>';
}else if($point<=7){
    echo '<div><h2>' . "Nhận xét: Cũng bình thường.".'</div>';
}else{
    echo '<div><h2>' . "Nhận xét: Sắp sửa làm được trợ giảng lớp PHP.".'</div>';
}

?>
